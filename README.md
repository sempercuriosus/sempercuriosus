# About Me
Hi 👋 My name is Eric! 

Here are a few things about me: 

## Bio

I have been coding learning what to do one project at a time since 2015 earning an AAS in Application Development, and currently I am learning how to be a Full Stack Web Developer through UNCC's Code Bootcamp.

🌍  I'm based on the East Coast, USA hailing from Wisconsin 🧀 originally.


🌱 Currently Learning: Full Stack Web Developer.


🖥️ My [Portfolio Webpage](https://github.com/sempercuriosus/PortfolioChallenge)

🖥️ My [Personal Webpage](http://thirdcoast.dev) (This is a work in progress and as of Oct 2023 I do not have anything up)

✉️  You can contact me via [Email](mailto:hulse@hey.com)

⚡  I enjoy learning, problem solving, finding new-to-me experiences, working with my hands in my rag-tag basement wood shop, or figuring out how to fix my cars or working on my house. I once fixed my water heater's gas leak sensor with a #2 pencil! The graphite from the pencil increased the sensor's resistance, getting it back into the right tolerance range, while the new one was on the way! Cold water in Wisconsin winter was not my favorite thing.

Outside of work, I love being out in nature be it on a hike, biking, walking, or just taking the time to sit in the grass and stare at the clouds floating on by. 

## Skills
These are some of the skills that I have acquired over the years, listed in no particular order

- C#
- JavaScript
- Python
- HTML
- CSS
- React
- NodeJS
- MongoDB
- Heroku
- .NET Core
- Git
- MacOS
- Docker
- Raspberry Pi

## Social Media

- I do not use social media but send me an email!
